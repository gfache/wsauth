FROM openjdk:8
ADD target/ws-auth.jar ws-auth.jar
EXPOSE 9390
ENTRYPOINT ["java", "-jar", "ws-auth.jar"]