package fr.afcepf.al33.dao.api;

import fr.afcepf.al33.entities.Authentification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDaoAuthentification extends JpaRepository<Authentification, Integer> {

    public Authentification findAuthentificationByLoginAndPassword(String login, String password);

    public Authentification findAuthentificationByLogin(String login);

    public Authentification findAuthentificationByIdKikatou(Integer idKika);

}
