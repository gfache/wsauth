package fr.afcepf.al33.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="idAuth", scope = Authentification.class)
public class Authentification implements Serializable {
	private static final long serialVersionUID = 1L;

    public Authentification() {
    }

    public Authentification(String login, String password, Integer idKikatou, Role role) {
        this.login = login;
        this.password = password;
        this.idKikatou = idKikatou;
        this.role = role;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected Integer idAuth;

    @Column(nullable = false, length = 50)
    protected String login;

    @Column(nullable = false, length = 150)
    protected String password;

    @Column(nullable = false)
    protected Integer idKikatou;

    @ManyToOne
    @JoinColumn(name="idRole")
    private Role role;

    public Integer getIdAuth() {
        return idAuth;
    }

    public void setIdAuth(Integer idAuth) {
        this.idAuth = idAuth;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIdKikatou() {
        return idKikatou;
    }

    public void setIdKikatou(Integer idKikatou) {
        this.idKikatou = idKikatou;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}