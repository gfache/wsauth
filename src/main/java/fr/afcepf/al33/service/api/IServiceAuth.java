package fr.afcepf.al33.service.api;

import fr.afcepf.al33.entities.Authentification;
import fr.afcepf.al33.entities.Role;

public interface IServiceAuth {

    Authentification ajouterAuth(Authentification authentification);
    void supprimerAuthentification(Integer idKikatou);
    Authentification modifierAuthentification(Authentification authentification);
    Authentification getAuthByIdKikatou(Integer id);
    Authentification connexion(String login, String password);
    Role getRoleByLibelle(String libelle);
}
